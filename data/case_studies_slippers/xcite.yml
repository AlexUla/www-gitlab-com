title: Xcite
file_name: xcite
canonical_path: /customers/xcite/
cover_image: /images/blogimages/xcite_cover_image.jpg
cover_title: |
  How X-Cite uses GitLab’s SCM for robust worldwide workflow
cover_description: |
  X-Cite’s product and development teams use GitLab for SCM, CI/CD, and performance agility.
twitter_image: /images/blogimages/xcite_cover_image.jpg
twitter_text: Learn how X-Cite uses GitLab’s SCM for robust worldwide workflow @XCITE_solutions
customer_logo: /images/case_study_logos/x-cite_logo.jpeg
customer_logo_css_class: brand-logo-tall
customer_industry: Technology
customer_location: Luxembourg, Belgrade, Tunisia
customer_solution: GitLab Premium SaaS
customer_employees: |
  50
customer_overview: |
  X-Cite was looking for a singular platform to provide customers with a high level of performance and exceptional quality.
customer_challenge: |
  X-Cite’s teams are spread around three countries,they needed one DevOps platform to unify their workflows
key_benefits: >-

  
    Kubernetes integration   

  
    Single source of truth

  
    End-to-end visibility

  
    Robust GCP integration

  
    Docker integration


    CI/CD capabilities
customer_stats:
  - stat: 250% 
    label: Reduction on CI run time (runs took 40-50 mins; now it's less than 10 mins)
  - stat:  10K USD p.a
    label: Annual cost savings from toolchain reduction and less operational overhead
  - stat:  15%
    label: Increase in developer productivity
customer_study_content:
  - title: the customer
    subtitle: Digital solutions enterprise
    content: >-
  
        X-Cite is a 5G, cloud, and IoT-centric company that was formed in 2018 in Luxembourg. [X-Cite](https://x-cite.io/about-us/) develops and provides customers with an integrated platform built on machine learning and AI, with a focus on connectivity, IoT, and machine-to-machine workflow. The company has recently expanded to two other locations, including Serbia and Tunisia.


  - title: the challenge
    subtitle:  Working together while working apart
    content: >-
    

        X-Cite developers are spread across three countries and are very rarely physically together to collaborate on projects. The primary form of communication is through virtual calls and development tools. “Through this setup, we already spend several thousands of hours on development. Having the right tool to have a team incorporating all together was a really, really important point,” according to Vincent Lekens, COO and Head of Business. The company needed a tool that would provide developers with a way to collaborate and manage code, and also offer visibility into each other’s projects. From a project management perspective, it was incredibly important to have access at any time to a project’s status and for all the teams’ tracking to live in one place. 

        Ideally, both the development team and product team could use the same platform. “The goal is to achieve a high level of performance and quality, of course, and deliver Agile projects that provide exceptional business standards for users,” said Jelena Lecic, PMO. X-Cite wanted to be able to focus on continuous migration, continuous testing, and continuous deployment, with a high degree of integration. In particular, developers needed an environment for Kubernetes to run services and to become more automated.


  - title: the solution
    subtitle:  Less overhead, more integration
    content: >-


        After researching a few other platforms, X-Cite adopted GitLab because of its superior integration capabilities. “We know how GitHub works, but one of the main differentiators was for us the high degree of integration [with GitLab]. From the initial user requirement or from capturing the user story with all the acceptance criteria, then the whole development cycle, finally we see the first deployment,” said Ulf Theobald, Partner, Executive Innovation & Technology Director. GitLab provides predefined configuration for CI/CD and several other environments, which is exactly what X-Cite needed to get up and running. “That was for us the trigger point for why we decided on GitLab. Simply to have as little overhead as possible throughout the development cycle in our development team, because they must focus on our code base and not on configuring tools of any third party,” Theobald said. 

      
        The development and product teams are both using GitLab. “We are using GitLab for documenting all the projects. We have our product backlog where we are documenting all the tickets and we are delivering frame by frame. We are using two-week frames for each incremental delivery. After every GitLab deployment sprint, we have something that is functional,” said Jelena Lecic, PMO. The project management team is now able to see a project from start to finish in one platform and the status of each using GitLab’s issues and boards. Teams are empowered as they are now able to plan for how long a project is expected to take and what the requirement will be. "I can at any moment see in the CI/CD pipeline what someone did and the last pushes they made. Team-tracking is something that is very good. That for me was, from a project management point of view, a very good tool to have everything in one place," Lecic explained.


        “We have a board for every project, which lists the backlog, so I can at any moment see where we are, who is assigned for which ticket, where we are progressing, because we have pre-planning, we estimate the ticket, we know how much time we will need for each of these. We can see at any time, by the end of the day or for our next daily meeting, I can see the progress, how much time they spent so far, and the estimation from the start,” Lecic added. 

      

  - title: the results
    subtitle:  Cloud-native integration, automated workflows, happy developers
    content: >-

        X-Cite now has all the benefits of several devices in one solution. The processes are cloud-native and fully automated. Without having deployments in GitLab, X-Cite considered being cloud-native “nearly unachievable” because the previous monolithic approach caused small, manual changes to have large, complicated steps that slowed workflows. “Of all the positive aspects you can have being cloud-native, you still need to have the right tooling, because otherwise, the promise will simply not become a reality behind cloud-native because you're simply having too much overhead on the operational side. That's why you need to have a high degree of automation,” Theobald said. 


        GitLab automates deployments and provides seamless integration, allowing for cloud-native capabilities without the previously experienced headaches. Teams are using the container registry inside GitLab to deploy applications into Google Cloud Platform. With Kubernetes using Istio and Knative, the teams use GitLab runner instances to execute the CI/CD process. According to Theobold, “From a complexity point of view, it was very straightforward, it's stable, it works. No complaints.” X-Cite now has code management in place, “straightforward” Kubernetes integration, and integrated CI/CD. On top of that, the product and development team members have a singular platform to work on projects together, despite being spread around the world. “If it is not in GitLab, it will not get done … GitLab is simply the single source of proof for the requirements and also for the acceptance criteria,” Theobald added.


        The development team has had an increase in deployments and is currently deploying around 10-12 times daily. “It's something the developers don't even recognize anymore, because it's completely frictionless. Sometimes they even forget the luxury they have compared to other tools like a Jenkins-based approach or something comparable, I mean, to something really old-fashioned,” Theobald said. Prior to GitLab deployment  pipelines needed close supervision and took up to 50 minutes to run; these are now automated and standardized with GitLab and take less than 10 minutes.  Pipeline set up time has reduced from 30 minutes to just 2-3 minutes thereby freeing up developer time to focus on code.


        Moreover, the ease of integration, the speed of throughput, and the overall workflow has given developers nothing to complain about. Productivity has increased by up to 15% with GitLab; developers onboard quickly and start contributing within days. “It's basically the only tool where I have until now never received a complaint from a developer. I have never received a complaint like ‘it doesn't work, it's too slow, I don't like it.’ As you know, developers are overall technical people. They can be very picky, but until now, we have never received a complaint,” Theobald added. 

   

        ## Learn more about GitLab solutions
    
  
        [Security with GitLab](/solutions/dev-sec-ops/)
    
  
        [CI with GitLab](/stages-devops-lifecycle/continuous-integration/)
    
  
        [Choose a plan that suits your needs](/pricing/)
customer_study_quotes:
  - blockquote: I can easily say we are saving on average $10,000 per annum on toolchain reduction and operational cost from moving to GitLab
    attribution: Ulf Theobald
    attribution_title: Partner, Executive Innovation & Technology Director at X-Cite S.A











